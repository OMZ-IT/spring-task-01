package com.medoc.reference_maladie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RefMaladieRepository extends JpaRepository<ReferenceMaladie,Integer> {
}
