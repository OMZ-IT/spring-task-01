package com.medoc.reference_maladie;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class RefMaladieService {
    private final RefMaladieRepository refMaladieRepository;


    public void saveAllRefMaladie(List<ReferenceMaladie> referenceMaladieList) {
        refMaladieRepository.saveAll(referenceMaladieList);
    }

}
