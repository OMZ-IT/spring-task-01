package com.medoc.reference_maladie;


import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "reference_maladie")
@EntityListeners(AuditingEntityListener.class)
public class ReferenceMaladie {

    @Id
    @GeneratedValue
    private Integer id;
    private Integer age;
    private String username;
}
