package com.medoc.MedPerson;


import com.medoc.user.User;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "_person_med")
@EntityListeners(AuditingEntityListener.class)
public class MedPerson {

    @Id
    @GeneratedValue
    private Integer id;
    private String nom;
    private String prenom;

    private final String medicament="DOLIPRANE";

    @ManyToOne
    @JoinColumn(name = "creator")
    User creator;
    @ManyToOne
    @JoinColumn(name = "updator")
    User updator;



    @CreatedDate
    @Column(nullable = false, updatable = false)
    private LocalDateTime createdDate;
    @LastModifiedDate
    @Column(insertable = false)
    private LocalDateTime lastModifiedDate;

}
