package com.medoc.MedPerson;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedPersonRepository extends JpaRepository<MedPerson,Integer> {
}
