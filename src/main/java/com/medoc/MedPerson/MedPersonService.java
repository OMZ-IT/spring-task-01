package com.medoc.MedPerson;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MedPersonService {
    private final MedPersonRepository medPersonRepository;

    public void saveAllMedPerson(List<MedPerson> medPersonList){
        medPersonRepository.saveAll(medPersonList);
    }

}
