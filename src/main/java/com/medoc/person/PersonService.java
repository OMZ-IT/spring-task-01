package com.medoc.person;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PersonService {
    private final PersonRepository personRepository;

    public List<Person> savePersons(List<Person> personList) {
        return personRepository.saveAll(personList);
    }

}
