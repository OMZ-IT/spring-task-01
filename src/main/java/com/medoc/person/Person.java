package com.medoc.person;


import com.medoc.user.User;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "_person")
@EntityListeners(AuditingEntityListener.class)
public class Person {
    @Id
    @GeneratedValue
    private Integer id;
    private String username;

    private String lieu;
    private LocalDate dateNaissance;
    @ManyToOne
    @JoinColumn(name = "creator")
    User creator;
    @ManyToOne
    @JoinColumn(name = "updator")
    User updator;



    @CreatedDate
    @Column(nullable = false, updatable = false)
    private LocalDateTime createdDate;
    @LastModifiedDate
    @Column(insertable = false)
    private LocalDateTime lastModifiedDate;

}
