package com.medoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedocProjetApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedocProjetApplication.class, args);
	}

}
