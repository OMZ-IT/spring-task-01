package com.medoc.person_maladie;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PersonMaladieService {

    private final PersonMaladieRepository personMaladieRepository;

    public void savePersonMaladie(List<PersonMaladie> personMaladieList){
        personMaladieRepository.saveAll(personMaladieList);
    }

}
