package com.medoc.person_maladie;



import com.medoc.person.Person;
import com.medoc.user.User;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "_person_maladie")
@EntityListeners(AuditingEntityListener.class)
public class PersonMaladie {
    @Id
    private Integer id;

    @OneToOne
    @MapsId
    @JoinColumn(name = "person_id", referencedColumnName = "id")
    private Person person;

    private String maladie="Angine";
    @ManyToOne
    @JoinColumn(name = "creator")
    User creator;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "updator",nullable = true)
    User updator;

    @CreatedDate
    @Column(nullable = false, updatable = false)
    private LocalDateTime createdDate;
    @LastModifiedDate
    @Column(insertable = false)
    private LocalDateTime lastModifiedDate;
}
