package com.medoc.person_maladie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonMaladieRepository extends JpaRepository<PersonMaladie,Integer> {
}
