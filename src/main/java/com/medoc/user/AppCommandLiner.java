package com.medoc.user;


import com.medoc.person.Person;
import com.medoc.person.PersonService;
import com.medoc.person_maladie.PersonMaladie;

import com.medoc.person_maladie.PersonMaladieService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@AllArgsConstructor
public class AppCommandLiner implements CommandLineRunner {

    private final UserService userService;
    private final PersonService personService;

    private final PersonMaladieService personMaladieService;


    @Override
    @Transactional
    public void run(String... args) throws Exception {
        LocalDateTime now = LocalDateTime.now();
        List<User> users = new ArrayList<>();
        List<Person> persons = new ArrayList<>();
        List<PersonMaladie> personMaladies = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            users.add(new User(null, "john_" + i, now, now));
        }
        List<User> userList = userService.saveUsers(users);

        for (int i = 0; i < 5; i++) {
            User relatedUser = users.get(i > 0 ? (int) Math.floor(Math.random() * i) : 0);
            Person person = new Person(null, "paul_" + i, "Batna", LocalDate.of(1990, Month.MAY, i + 1), users.get(i), relatedUser, now, now);
            persons.add(person);
        }
        List<Person> personList = personService.savePersons(persons);


        for (int i = 0; i < 5; i++) {
            personMaladies.add(new PersonMaladie(null, personList.get(i), "", userList.get(i), null, now, now));
            /* personMaladies.add(new PersonMaladie(null, person,maladieList.get(2)));*/
        }
        personMaladieService.savePersonMaladie(personMaladies);
    }
}
