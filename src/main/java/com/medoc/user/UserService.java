package com.medoc.user;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public List<User> saveUsers(List<User> users) {
       /* this.userRepository.save(user);*/
    return userRepository.saveAll(users);
    }

}
